import React from 'react'
import * as utils from '../shared/utils'

const isFunc = thing => typeof thing === 'function'
export default class Validator extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.isValid = this.isValid.bind(this)
    this.isEnabled = this.isEnabled.bind(this)
    this.reason = this.reason.bind(this)
  }

  isValid() {
    return isFunc(this.props.isValid) ? this.props.isValid() : this.props.isValid
  }

  isEnabled() {
    return isFunc(this.props.isEnabled) ? this.props.isEnabled() : this.props.isEnabled
  }

  reason() {
    return isFunc(this.props.reason) ? this.props.reason() : this.props.reason
  }

  render() {
    if (!this.props.isEnabled) return null
    return React.cloneElement((
      <span title={!this.isValid() ? this.reason() : ''}>
        {this.isValid()
          ? <i style={{color: 'green'}} className="fa fa-check" aria-hidden="true"></i>
          : <i style={{color: 'red'}} className="fa fa-times" aria-hidden="true"></i>}
      </span>), utils.cloneWithoutProperties(this.props, Object.keys(Validator.propTypes)))
  }
}

Validator.propTypes = {
  isValid: React.PropTypes.any,
  isEnabled: React.PropTypes.any,
  reason: React.PropTypes.oneOfType([React.PropTypes.func, React.PropTypes.string]),
  children: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(React.PropTypes.node),
    React.PropTypes.node
  ]),
}