import React from 'react'

class SCCAPanel extends React.Component {
  constructor(props, context) {
    super(props, context)
  }

  render() {
    return (
      <div className="panel boxed">
        <div className="panel-heading boxed-header">{this.props.name}</div>
        <div className="panel-content boxed-content">
          {this.props.children}
          <div className="clearfix"></div>
        </div>
        <div className="panel-footer boxed-footer">
          <div className="clearfix"></div>
        </div>
      </div>
    )
  }
}

SCCAPanel.propTypes = {
  children: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(React.PropTypes.node),
    React.PropTypes.node
  ]),
  name: React.PropTypes.string.isRequired
}

export default SCCAPanel