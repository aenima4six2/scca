import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import '../../../server/public/stylesheets/animations.css'

class Animator extends React.Component {
  constructor(props, context) {
    super(props, context)
  }

  render() {
    const path = this.props.location.pathname
    const segment = path.split('/')[2] || 'root'
    return (
      <ReactCSSTransitionGroup
        component="div"
        transitionName={segment === 'root' ? 'inFromLeft' : 'inFromRight'}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={600}>
        {React.cloneElement(this.props.children, {key: path})}
      </ReactCSSTransitionGroup>
    )
  }
}

Animator.propTypes = {
  children: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(React.PropTypes.node),
    React.PropTypes.node
  ]),
  location: React.PropTypes.object
}

export default Animator