import React from 'react'
import {Link} from 'react-router'
import ReduxToastr from 'react-redux-toastr'
import Animator from './shared/Animator'

class App extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {navbarToggled: false}
    this.toggleNavBar = this.toggleNavBar.bind(this)
  }

  toggleNavBar() {
    this.setState({navbarToggled: !this.state.navbarToggled})
  }

  render() {
    return (
      <div>
        <ReduxToastr/>
        <header className="navbar navbar-inverse navbar-fixed-top">
          <nav className="navbar-inner">
            <div className="container">
              <div className="nav pull-left"/>
              <div className="nav pull-right">
                <ul id="user_bar" className="nav">
                  <li>
                    <a href="https://www.scca.com/login" title="Log in to Sports Car Club of America.">
                      <i className="fa fa-sign-in"/> Log in
                    </a>
                  </li>
                  <li>
                    <a href="https://www.scca.com/signup" title="Sign up to Sports Car Club of America.">
                      <i className="fa fa-user"/> Sign up
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </header>
        <div id="site_header">
          {/** Header */}
          <div className="site_logo_row">
            <div className="container">
              <div className="outside_left">
                <div className="stripe_left"/>
                <div className="stripe_right"/>
              </div>
              <div id="site_logo_container" className="row-fluid">
                <div className="site_logo span4">
                  <a href="http://www.scca.com">
                    <img alt="Sports Car Club of America" src="/images/logo.png" className="logo"/>
                  </a>
                </div>
              </div>
              <div className="outside_right"/>
            </div>
          </div>
          {/** Nav */}
          <div id="site_nav_container" className="row-fluid">
            <div className="container">
              <div id="site_nav" className="navbar">
                <div className="navbar-inner">
                  <a href="https://www.scca.com/" className="brand">
                    <img alt="Sports Car Club of America" className="logo" src="/images/medium.png"></img>
                  </a>
                  <a className="btn btn-navbar" onClick={this.toggleNavBar}>
                    <i className="fa fa-bars"></i> Menu
                  </a>
                  <div className="nav-collapse" style={this.state.navbarToggled ? {height: 'auto'} : null}>
                    <ul className="nav main_menu" id="main_menu">
                      <li>
                        <Link to="/"><span>Car Class Lookup</span></Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main">
          <div className="container">
            <div id="main_content" className="row-fluid">
              <div className="content_container">
                <div id="content" className="span8 offset2">
                  <div id="page_header" className="page-header">
                    <h2>Header</h2>
                  </div>
                  <div id="page_content">
                    <Animator location={this.props.location}>
                      {this.props.children}
                    </Animator>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

App.propTypes = {
  children: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(React.PropTypes.node),
    React.PropTypes.node
  ]),
  location: React.PropTypes.object
}

export default App