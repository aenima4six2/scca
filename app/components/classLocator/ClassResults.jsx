import React from 'react'
import Panel from '../shared/SCCAPanel'
import ClassResult from './ClassResult'
import {connect} from 'react-redux'
import * as actions from '../../actions/classLookupActions'
import {Link} from 'react-router'
import style from './ClassResult.css'

class ClassResults extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.renderResults = this.renderResults.bind(this)
    this.renderLoading = this.renderLoading.bind(this)
  }

  componentDidMount() {
    this.props.dispatch(actions.reset())
    if (this.props.make && this.props.model) {
      this.props.dispatch(actions.getClassesFromApi({
        make: this.props.make.name,
        model: this.props.model.name,
        start: this.props.start || 'all',
        end: this.props.end || 'all'
      }))
    }
    else this.props.dispatch(actions.setClasses([]))
  }

  renderResults() {
    return this.props.classes.length ? (
      <div>
        {
          this.props.classes.map((x, idx) => {
            return (
              <ClassResult key={idx} class={x.class} make={x.make}
                           model={x.model} start={x.start} end={x.end}
                           notes={x.notes}/>
            )
          })
        }
      </div>
    ) : <div><p>No results found!</p></div>
  }

  renderLoading() {
    return (
      <div style={{textAlign: 'center'}}>
        <p>Loading...</p>
        <img src={'/images/loading.gif'} alt={'loading'}/>
      </div>
    )
  }

  render() {
    return (
      <Panel name="Vehicle Class Results">
        <div className={style.container}>
          {this.props.classesLoaded ? this.renderResults() : this.renderLoading()}
          <Link to="/">
            <input type="button" value="Back" className="btn btn-primary"/>
          </Link>
        </div>
      </Panel>
    )
  }
}

ClassResults.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  classes: React.PropTypes.array.isRequired,
  classesLoaded: React.PropTypes.bool,
  make: React.PropTypes.object,
  model: React.PropTypes.object,
  start: React.PropTypes.string,
  end: React.PropTypes.string
}

function mapStateToProps(state) {
  return {
    classes: state.classLocator.classes,
    classesLoaded: state.classLocator.classesLoaded,
    make: state.classLocator.selectedMake,
    model: state.classLocator.selectedModel,
    start: state.classLocator.selectedStartYear,
    end: state.classLocator.selectedEndYear
  }
}

export default connect(mapStateToProps)(ClassResults)