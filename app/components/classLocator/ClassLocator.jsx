import React from 'react'
import Panel from '../shared/SCCAPanel'
import MakeSuggester from './MakeSuggester'
import ModelSuggester from './ModelSuggester'
import {Link} from 'react-router'
import {connect} from 'react-redux'
import YearRange from './YearRange'

class ClassLocator extends React.Component {
  constructor(props, context) {
    super(props, context)
  }

  render() {
    return (
      <Panel name="Vehicle Class Locator">
        <form>
          <div className="form-horizontal">
            <div className="control-group">
              <label htmlFor="make" className="control-label">Make</label>
              <MakeSuggester id="makes"/>
            </div>
            <div className="control-group">
              <label htmlFor="model" className="control-label">Model</label>
              <ModelSuggester id="models"/>
            </div>
            <div className="control-group">
              <label htmlFor="year" className="control-label">Year</label>
              <YearRange/>
            </div>
            <div className="control-group">
              <label htmlFor="mods" className="control-label">Modifications</label>
              <div id="mods" className="controls">
                <label className="checkbox">
                  <input disabled={this.props.disabled} type="checkbox"/> ECM Tuning
                </label>
                <label className="checkbox">
                  <input disabled={this.props.disabled} type="checkbox"/> Bolt Ons
                </label>
                <label className="checkbox">
                  <input disabled={this.props.disabled} type="checkbox"/> Power Adders
                </label>
                <Link to="/results">
                  <input type="button" value="Search" className="btn btn-primary" disabled={this.props.disabled}/>
                </Link>
              </div>
            </div>
          </div>
        </form>
      </Panel>
    )
  }
}

ClassLocator.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  disabled: React.PropTypes.bool
}

function mapStateToProps(state) {
  return {
    disabled: !(state.classLocator.selectedMake && state.classLocator.selectedModel)
  }
}

export default connect(mapStateToProps)(ClassLocator)