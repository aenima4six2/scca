import React from 'react'
import Autosuggest from 'react-autosuggest'
import {connect} from 'react-redux'
import * as actions from '../../actions/classLookupActions'
import theme from '../shared/SuggesterTheme.css'
import style from './MakeSuggester.css'
import Validator from '../shared/Validator'

class MakeSuggester extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.onChange = this.onChange.bind(this)
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this)
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this)
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this)
    this.isInputValid = this.isInputValid.bind(this)
    this.state = {value: ''}
  }

  onChange(event, {newValue}) {
    this.setState({value: newValue})
    this.props.dispatch(actions.selectMake())
  }

  onSuggestionsFetchRequested({value}) {
    this.props.dispatch(actions.getMakesFromApi(value))
  }

  onSuggestionsClearRequested() {
    this.props.dispatch(actions.setMakes([]))
  }

  onSuggestionSelected(event, {suggestion}) {
    this.props.dispatch(actions.selectMake(suggestion))
  }

  isInputValid() {
    return this.props.selectedMake && this.props.selectedMake.name === this.state.value
  }

  renderSuggestion(make) {
    return (
      <span className={style.suggestionContent} style={{backgroundImage: `url(${make.uri})`}}>
            <span>{make.name}</span>
      </span>
    )
  }

  render() {
    const inputProps = {
      placeholder: 'Type a make',
      value: this.state.value,
      onChange: this.onChange
    }

    return (
      <div className="controls">
        <Autosuggest
          id={this.props.id}
          theme={theme}
          focusFirstSuggestion={true}
          suggestions={this.props.makes}
          shouldRenderSuggestions={() => true}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={suggestion => suggestion.name}
          renderSuggestion={this.renderSuggestion}
          onSuggestionSelected={this.onSuggestionSelected}
          inputProps={inputProps}
        />
        <Validator
          className={style.validator}
          isEnabled={this.state.value}
          isValid={this.isInputValid}
          reason={() => `\"${this.state.value}\" is not a valid make!`}
        />
      </div>
    )
  }
}

MakeSuggester.propTypes = {
  id: React.PropTypes.string.isRequired,
  selectedMake: React.PropTypes.object,
  makes: React.PropTypes.array.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    selectedMake: state.classLocator.selectedMake,
    makes: state.classLocator.makes
  }
}

export default connect(mapStateToProps)(MakeSuggester)