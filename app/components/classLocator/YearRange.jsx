import React from 'React'
import * as actions from '../../actions/classLookupActions'
import {connect} from 'react-redux'

class YearRange extends React.Component {
  render() {
    return (
      <div id="year" className="form-inline controls">
        <input type="text" placeholder="From" id="from" className="input-mini"
               disabled={this.props.disabled}
               value={this.props.selectedStartYear}
               onChange={e => {
                 const value = e.target.value.toString()
                 if (value === '' || value.match(/^\d*$/)) {
                   this.props.dispatch(actions.selectYearStart(value))
                 }
               }}/>
        <input type="text" placeholder="To" id="to" className="input-mini"
               disabled={this.props.disabled}
               value={this.props.selectedEndYear}
               onChange={e => {
                 const value = e.target.value.toString()
                 if (value === '' || value.match(/^\d*$/)) {
                   this.props.dispatch(actions.selectYearEnd(value))
                 }
               }}/>
      </div>
    )
  }
}

YearRange.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  disabled: React.PropTypes.bool,
  selectedEndYear: React.PropTypes.string,
  selectedStartYear: React.PropTypes.string
}

function mapStateToProps(state) {
  return {
    disabled: !(state.classLocator.selectedMake && state.classLocator.selectedModel),
    selectedEndYear: state.classLocator.selectedEndYear,
    selectedStartYear: state.classLocator.selectedStartYear
  }
}

export default connect(mapStateToProps)(YearRange)