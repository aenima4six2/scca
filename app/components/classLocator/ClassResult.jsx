import React from 'react'
import style from './ClassResult.css'

class ClassResults extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.parseClassAbbreviation = this.parseClassAbbreviation.bind(this)
  }

  parseClassAbbreviation() {
    const pattern = /\((\w+)\)$/g
    const match = pattern.exec(this.props.class)
    return match[1]
  }

  render() {
    return (
      <div className="well well-large">
        <div className={style.classCardIcon}>{this.parseClassAbbreviation()}</div>
        <div className={style.classCardText}>
          <div className={style.classCardTextHeader}>
            <span>{this.props.class}</span>
          </div>
          <div className={style.classCardTextBody}>
            <div>
              <span className={`label ${style.classCardTextLabel}`}>Make:</span>
              {this.props.make}
            </div>
            <div>
              <span className={`label ${style.classCardTextLabel}`}>Model:</span>
              {this.props.model}
            </div>
            <div>
              <span className={`label ${style.classCardTextLabel}`}>Years:</span>
              {
                this.props.start && this.props.end
                  ? `${this.props.start} - ${this.props.end}`
                  : this.props.start || this.props.end || 'All'
              }
            </div>
            <div>
              <span className={`label ${style.classCardTextLabel}`}>Notes:</span>
              {this.props.notes || 'None'}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ClassResults.propTypes = {
  class: React.PropTypes.string.isRequired,
  make: React.PropTypes.string.isRequired,
  model: React.PropTypes.string.isRequired,
  start: React.PropTypes.string,
  end: React.PropTypes.string,
  notes: React.PropTypes.string
}

export default ClassResults