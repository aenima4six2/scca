import React from 'react'
import Autosuggest from 'react-autosuggest'
import {connect} from 'react-redux'
import * as actions from '../../actions/classLookupActions'
import theme from '../shared/SuggesterTheme.css'
import style from './ModelSuggester.css'
import Validator from '../shared/Validator'

class MakeSuggester extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.onChange = this.onChange.bind(this)
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this)
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this)
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this)
    this.isInputValid = this.isInputValid.bind(this)
    this.state = {value: ''}
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.selectedMake) {
      this.setState({value: ''})
      this.props.dispatch(actions.selectModel())
    }
  }

  onChange(event, {newValue}) {
    this.setState({value: newValue})
    this.props.dispatch(actions.selectModel())
  }

  onSuggestionsFetchRequested({value}) {
    const makeName = this.props.selectedMake ? this.props.selectedMake.name : 'all'
    this.props.dispatch(actions.getModelsFromApi(makeName, value))
  }

  onSuggestionsClearRequested() {
    this.props.dispatch(actions.setModels([]))
  }

  onSuggestionSelected(event, {suggestion}) {
    this.props.dispatch(actions.selectModel(suggestion))
  }

  isInputValid() {
    return this.props.selectedModel && this.props.selectedModel.name === this.state.value
  }

  render() {
    const inputProps = {
      placeholder: 'Type a model',
      value: this.state.value,
      onChange: this.onChange,
      disabled: !this.props.selectedMake
    }

    return (
      <div className="controls">
        <Autosuggest
          id={this.props.id}
          theme={theme}
          focusFirstSuggestion={true}
          suggestions={this.props.models}
          shouldRenderSuggestions={() => true}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={suggestion => suggestion.name}
          renderSuggestion={suggestion => <span>{suggestion.name}</span>}
          onSuggestionSelected={this.onSuggestionSelected}
          inputProps={inputProps}
        />
        <Validator
          className={style.validator}
          isEnabled={this.state.value}
          isValid={this.isInputValid}
          reason={() => `\"${this.state.value}\" is not a valid model!`}
        />
      </div>

    )
  }
}

MakeSuggester.propTypes = {
  id: React.PropTypes.string.isRequired,
  selectedMake: React.PropTypes.object,
  selectedModel: React.PropTypes.object,
  models: React.PropTypes.array.isRequired,
  dispatch: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    selectedMake: state.classLocator.selectedMake,
    selectedModel: state.classLocator.selectedModel,
    models: state.classLocator.models
  }
}

export default connect(mapStateToProps)(MakeSuggester)