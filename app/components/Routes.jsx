import React from 'react'
import {Router, Route, IndexRoute, browserHistory} from 'react-router'
import App from './App'
import ClassLocator from './classLocator/ClassLocator'
import ClassResults from './classLocator/ClassResults'

const Routes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ClassLocator}/>
      <Route path="/results" component={ClassResults}/>
    </Route>
  </Router>
)

export default Routes