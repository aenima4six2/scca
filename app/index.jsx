import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import Routes from './components/Routes'
import configureStore from './store/configureStore'
import 'react-redux-toastr/src/styles/index.scss'
import '../server/public/stylesheets/scca2.css'
import '../server/public/stylesheets/scca.css'
import '../server/public/stylesheets/font-awesome.css'
import '../server/public/stylesheets/overrides.css'

const store = configureStore()
render((
  <Provider store={store}>
    <Routes/>
  </Provider>
), document.getElementById('app'))