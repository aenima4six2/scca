export const MAKE_SUGGESTER_SEARCH = 'CLASS-LOOKUP:MAKE_SUGGESTER_SEARCH'
export const MAKE_SELECTED = 'CLASS-LOOKUP:MAKE_SELECTED'
export const MODEL_SUGGESTER_SEARCH = 'CLASS-LOOKUP:MODEL_SUGGESTER_SEARCH'
export const MODEL_SELECTED = 'CLASS-LOOKUP:MODEL_SELECTED'
export const YEAR_START_SELECTED = 'CLASS-LOOKUP:YEAR_START_SELECTED'
export const YEAR_END_SELECTED = 'CLASS-LOOKUP:YEAR_END_SELECTED'
export const CLASSES_SEARCHED = 'CLASS-LOOKUP:CLASSES_SEARCHED'
export const RESET = 'CLASS-LOOKUP:RESET'