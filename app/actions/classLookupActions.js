'use strict'
import * as actions from './actionTypes'
import fetch from 'isomorphic-fetch'

export function setMakes(makes) {
  return {
    type: actions.MAKE_SUGGESTER_SEARCH,
    makes
  }
}

export function getMakesFromApi(make) {
  return dispatch => {
    return Promise.resolve()
      .then(() => fetchJson(`/api/vehicles/makes?name=${encodeURIComponent(make)}`))
      .then(json => dispatch(setMakes(json)))
  }
}

export function selectMake(make) {
  return {
    type: actions.MAKE_SELECTED,
    make
  }
}

export function setModels(models) {
  return {
    type: actions.MODEL_SUGGESTER_SEARCH,
    models
  }
}

export function getModelsFromApi(make, model) {
  return dispatch => {
    return Promise.resolve()
      .then(() => {
        const uriMake = encodeURIComponent(make)
        const uriModel = encodeURIComponent(model)
        return fetchJson(`/api/vehicles/makes/${uriMake}/models?name=${uriModel}`)
      })
      .then(json => dispatch(setModels(json)))
  }
}

export function selectModel(model) {
  return {
    type: actions.MODEL_SELECTED,
    model
  }
}

export function selectYearStart(year) {
  return {
    type: actions.YEAR_START_SELECTED,
    year
  }
}

export function selectYearEnd(year) {
  return {
    type: actions.YEAR_END_SELECTED,
    year
  }
}

  export function getClassesFromApi({make, model, start, end}) {
  return dispatch => {
    return Promise.resolve()
      .then(() => {
        const encMake = encodeURIComponent(make)
        const encModel = encodeURIComponent(model)
        const encStart = encodeURIComponent(start)
        const encEnd = encodeURIComponent(end)
        return fetchJson(`/api/vehicles/makes/${encMake}/models/${encModel}?start=${encStart}&end=${encEnd}`)
      })
      .then(json => dispatch(setClasses(json)))
  }
}

export function setClasses(classes) {
  return {
    type: actions.CLASSES_SEARCHED,
    classes
  }
}

export function reset() {
  return {
    type: actions.RESET
  }
}

function fetchJson(uri) {
  return Promise.resolve()
    .then(() => fetch(uri))
    .then(resp => {
      if (resp.statusCode < 200 || resp.statusCode > 299) {
        throw new Error('Invalid server response code')
      }

      return resp.json()
    })
}