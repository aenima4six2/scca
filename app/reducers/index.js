import {combineReducers} from 'redux'
import classLocator from './classLocatorReducer'
import {reducer as toastr} from 'react-redux-toastr'

const rootReducer = combineReducers({
  classLocator,
  toastr
})

export default rootReducer
