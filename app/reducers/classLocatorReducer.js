'use strict'
import * as actions from '../actions/actionTypes'
const initialState = {
  makes: [],
  selectedMake: null,

  models: [],
  selectedModel: null,

  selectedEndYear: '',
  selectedStartYear: '',

  classes: [],
  classesLoaded: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actions.MAKE_SUGGESTER_SEARCH: {
      return Object.assign({}, state, {
        makes: action.makes
      })
    }
    case actions.MODEL_SUGGESTER_SEARCH: {
      return Object.assign({}, state, {
        models: action.models
      })
    }
    case actions.MAKE_SELECTED: {
      return Object.assign({}, state, {
        selectedMake: action.make
      })
    }
    case actions.MODEL_SELECTED: {
      return Object.assign({}, state, {
        selectedModel: action.model
      })
    }
    case actions.YEAR_END_SELECTED: {
      return Object.assign({}, state, {
        selectedEndYear: action.year
      })
    }
    case actions.YEAR_START_SELECTED: {
      return Object.assign({}, state, {
        selectedStartYear: action.year
      })
    }
    case actions.CLASSES_SEARCHED: {
      return Object.assign({}, state, {
        classes: action.classes,
        classesLoaded: true
      })
    }
    case actions.RESET: {
      return Object.assign({}, initialState)
    }
    default: {
      return state
    }
  }
}