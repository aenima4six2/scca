#SCCA Car Class Lookup#

### What is this repository for? ###

* This is a simple application for identifying and looking up SCCA car class by vehicle.
* 0.1.0

### Requirements ###

* node.js v6.9.1 or later and npm
* docker & docker-compose
* Text editor or IDE of your choice (IE: WebStorm or VSCode)

### How do I get set up? ###

* Download source: `git clone`
* Setup -> `cd <path/to/repo/>`
* Fetch Dependencies -> `npm i`
* Star MongoDb -> `docker-compose up -d`
* Start Node -> `npm start (or node .)`

### Who do I talk to? ###
* jpdienst@gmail.com

### Example ###
* [Screencast](http://www.screencast.com/t/EUX17eH8T05J)