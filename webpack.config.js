module.exports = {
  debug: true,
  target: 'web',
  entry: './app/index.jsx',
  output: {
    path: `${__dirname}/server/dist`, // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/', // Use absolute paths to avoid the way that URLs are resolved by Chrome when they're parsed from a dynamically loaded CSS blob. Note: Only necessary in Dev.
    filename: 'bundle.js'
  },
  resolve: {
    root: [`${__dirname}/app`],
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {test: /.jsx?$/, exclude: /node_modules/, loaders: ['babel']},
      {test: /\.eot(\?v=\d+.\d+.\d+)?$/, loader: 'file'},
      {test: /\.(woff|woff2)(\?.*$|$)/, loader: 'file-loader?prefix=font/&limit=5000'},
      {test: /\.ttf(\?v=\d+.\d+.\d+)?$/, loader: 'file-loader?limit=10000&mimetype=application/octet-stream'},
      {test: /\.svg(\?v=\d+.\d+.\d+)?$/, loader: 'file-loader?limit=10000&mimetype=image/svg+xml'},
      {test: /\.(jpe?g|png|gif)$/i, loaders: ['file']},
      {test: /\.ico$/, loader: 'file-loader?name=[name].[ext]'},
      {test: /\.less$/, loader: 'style!css!less'},
      {test: /\.scss$/, loaders: ['style', 'css', 'resolve-url', 'sass?sourceMap']},
      {test: /public\/stylesheets\/.*\.css$/, loader: 'style!css!resolve-url'},
      {
        test: /app\/components.*\.css$/,
        loader: 'style!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
      }
    ]
  }
}
