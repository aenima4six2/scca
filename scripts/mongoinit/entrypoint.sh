#!/usr/bin/env bash
echo "Loading SCCA Class data"
cd /scripts
mongoimport --host mongodb:27017 -d scca -c classes --type csv --file classes.csv --headerline