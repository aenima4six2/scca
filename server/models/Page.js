'use strict'
class Page {
  /**
   * Creates a new Page
   * @param skip
   * @param take
   * @param total
   */
  constructor(skip, take, total) {
    this._skip = skip || 0
    this._take = take || 30
    this._total = total || take
  }

  get skip() {
    return this._skip
  }

  set skip(value) {
    this._skip = value
  }

  get take() {
    return this._take
  }

  set take(value) {
    this._take = value
  }

  get total() {
    return this._total
  }

  set total(value) {
    this._total = value
  }

  /**
   * Gets teh page count
   * @returns {number}
   */
  get pageCount() {
    if (this.take == 0) {
      return 0
    }

    const total = this.total % this.take == 0
      ? (this.total / this.take)
      : (this.total / this.take + 1)

    return total
  }

  /**
   * Gets the current page number
   * @returns {number}
   */
  get currentPageNumber() {
    if (this.take == 0) {
      return 0
    }

    return this.skip / this.take + 1
  }

  /**
   * Gets the next Page
   * @returns {Page}
   */
  get nextPage() {
    if (this.currentPageNumber < this.pageCount) {
      return new Page(this.skip + this.take, this.take, this.total)
    }
    else {
      return this
    }
  }

  /**
   * Gets the previous Page
   * @returns {Page}
   */
  get previousPage() {
    if (this.currentPageNumber > 1) {
      return new Page(this.skip - this.take, this.take, this.total)
    }
    else {
      return this
    }
  }

  /**
   * Gets the first Page
   * @returns {Page}
   */
  get firstPage() {
    if (this.currentPageNumber == 1) {
      return this
    }
    else {
      return new Page(0, this.take, this.total)
    }
  }

  /**
   * Gets the last Page
   * @returns {Page}
   */
  get lastPage() {
    if (this.currentPageNumber == this.pageCount) {
      return this
    }
    else {
      return new Page(this.pageCount * this.take - this.take, this.take, this.total)
    }
  }

  /**
   * Converts the Page to a string
   * @returns {*}
   */
  toString() {
    return String.format('Page %s of %s', this.getCurrentPageNumber(), this.getPageCount())
  }
}

module.exports = Page
