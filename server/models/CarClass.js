'use strict'
const config = require('config')
const mongoConfig = config.get('mongo')
const uri = mongoConfig['connection-uri']
const logger = require('../logging/jsonLogger')
const mongoose = require('mongoose')
const debugMode = process.env.NODE_ENV === 'development' ? true : false
mongoose.set('debug', debugMode)
mongoose.Promise = global.Promise
mongoose.connect(uri)
const db = mongoose.connection

db.on('error', err => logger.error('MongoDb error', err))
db.once('open', () => logger.info(`Connected to MongoDb at -> ${uri}`))

const schema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  class: String,
  make: String,
  model: String,
  start: mongoose.Schema.Types.Mixed,
  end: mongoose.Schema.Types.Mixed,
  page: mongoose.Schema.Types.Mixed,
  source: String,
  notes: String,
  created: {type: Date, default: Date.now},
  modified: {type: Date, default: Date.now},
  nonce: {type: mongoose.Schema.Types.ObjectId, default: new mongoose.Types.ObjectId}
})

module.exports = mongoose.model('classes', schema)