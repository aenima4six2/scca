'use strict'
const express = require('express')
const router = express.Router()
const CarClass = require('../models/CarClass')
const logger = require('../logging/jsonLogger')
const defaultLimit = 1000
const fs = require('fs')
const path = require('path')

router
  .get('/makes', (req, res, next) => {
    Promise.resolve()
      .then(() => {
        const name = req.query.name
        const query = name ? {make: startsWith(name)} : {}
        const limit = req.query.limit || defaultLimit
        CarClass.find(query).distinct('make').then(makes => {
          const results = [].concat(makes.sort())
            .map(name => ({name: String(name), uri: getLogo(name)}))
            .filter((x, idx) => idx <= limit)
          res.json(results)
        })
      })
      .catch(next)
  })
  .get('/makes/all/models', (req, res, next) => {
    Promise.resolve()
      .then(() => {
        const name = req.query.name
        const query = name ? {model: startsWith(name)} : {}
        const limit = req.query.limit || defaultLimit
        CarClass.find(query).distinct('model').then(models => {
          const results = [].concat(models.sort())
            .map(name => ({name: String(name)}))
            .filter((x, idx) => idx <= limit)
          res.json(results)
        })
      })
      .catch(next)
  })
  .get('/makes/:make/models', (req, res, next) => {
    Promise.resolve()
      .then(() => {
        const query = {make: anyCase(req.params.make)}
        const limit = req.query.limit || defaultLimit
        if (req.query.name) query.model = startsWith(req.query.name)
        CarClass.find(query).distinct('model').then(models => {
          const results = [].concat(models.sort())
            .map(name => ({name: String(name)}))
            .filter((x, idx) => idx <= limit)
          res.json(results)
        })
      })
      .catch(next)
  })
  .get('/makes/:make/models/:model', (req, res, next) => {
    Promise.resolve()
      .then(() => {
        const limit = req.query.limit || defaultLimit
        const query = {make: anyCase(req.params.make), model: anyCase(req.params.model)}
        const sort = {make: 1, model: 1}
        const $and = createAndQuery(req.query.start, req.query.end)
        if ($and && $and.length) query.$and = $and
        logger.debug(`Query -> ${JSON.stringify(query)}`)
        CarClass.find(query).limit(limit).sort(sort).then(models => {
          res.json(models)
        })
      })
      .catch(next)
  })

function createAndQuery(start, end) {
  const and = []
  if (start && start !== 'all') {
    start = parseInt(start)
    and.push({$or: IsNullNotExistsEmptyOr('start', {$lte: start})})
    and.push({$or: IsNullNotExistsEmptyOr('end', {$gte: start})})
  }
  if (end && end !== 'all') {
    end = parseInt(end)
    and.push({$or: IsNullNotExistsEmptyOr('start', {$lte: end})})
    and.push({$or: IsNullNotExistsEmptyOr('end', {$gte: end})})
  }

  return and
}

function IsNullNotExistsEmptyOr(field, query) {
  return [
    {[field]: ''},
    {[field]: null},
    {[field]: {$exists: false}},
    {[field]: query}
  ]
}

function startsWith(value) {
  return {$regex: new RegExp(`^${escape(value)}.*`, 'i')}
}

function anyCase(value) {
  return {$regex: new RegExp(`^${escape(value)}$`, 'i')}
}

function escape(input) {
  return input.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
}

function getLogo(name) {
  if (name) {
    const uri = `/images/logos/${encodeURIComponent(name.toLowerCase().replace(' ', '-'))}.jpg`
    const diskUri = path.join(__dirname, '../public', uri)
    if (fs.existsSync(diskUri)) return uri
  }

  return ''
}

module.exports = router
