'use strict'
module.exports.create = (winston, expressWinston) => {
    return expressWinston.errorLogger({
        transports: [
            new winston.transports.Console({
                json: false,
                colorize: true
            })
        ]
    })
}
