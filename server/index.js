const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const winston = require('winston')
const expressWinston = require('express-winston')
const logger = require('./logging/expressLogger')
const errLogger = require('./logging/expressErrorLogger')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const app = express()
const faviconPath = path.join(__dirname, 'public/images/original.png')

// Express setup
app.use(favicon(faviconPath))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}))
app.use(cookieParser())

// express-winston logger makes sense BEFORE the router.
app.use(logger.create(winston, expressWinston))

// Routes (After Logger)
app.use('/images/original.png', express.static(faviconPath))
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'dist')))
app.use('/api/vehicles', require('./routes/vehicles'))
app.use('*', require('./routes/index'))

// express-winston errorLogger makes sense AFTER the router.
app.use(errLogger.create(winston, expressWinston))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stack traces leaked to user
app.use(function (err, req, res) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})


module.exports = app
